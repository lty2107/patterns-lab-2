package FactoryMethod;

import FactoryMethod.factory.CoffeeFactory;
import FactoryMethod.factory.CoffeeGrade;
import FactoryMethod.factory.MyCoffee;

public class Main {
    public static void main(String[] args) throws IllegalAccessException {
        CoffeeFactory coffeeFactory = new CoffeeFactory();
        MyCoffee coffee = coffeeFactory.prepareCoffee(CoffeeGrade.Americano);
        coffee.grindCoffee();
        coffee.cookCoffee();
        coffee.pourCoffee();
        coffee.serveCoffee();
    }
}
