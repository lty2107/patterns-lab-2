package FactoryMethod.factory;

public interface Coffee {
    void grindCoffee();
    void cookCoffee();
    void pourCoffee();
    void serveCoffee();
}
