package FactoryMethod.factory;

public class MyCoffee implements Coffee{
    private final String myCoffee;

    public MyCoffee (String myCoffee){
        this.myCoffee = myCoffee;
    }

    @Override
    public void grindCoffee() {
        System.out.println("Мелим ваш " + myCoffee);
    }

    @Override
    public void cookCoffee() {
        System.out.println("Варим ваш " + myCoffee);
    }

    @Override
    public void pourCoffee() {
        System.out.println("Налием ваш " + myCoffee);
    }

    @Override
    public void serveCoffee() {
        System.out.println("Ваш " + myCoffee + " подан!");
    }
}
