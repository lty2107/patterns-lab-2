package FactoryMethod.factory;

public enum CoffeeGrade{
    Americano("Американо"){},
    Cappuccino("Капучино"),
    Espresso("Эспрессо"),
    Latte("Латте"),
    Frappe("Фраппе");

    private final String coffeeGrade;

    CoffeeGrade(String coffeeGrade){
        this.coffeeGrade = coffeeGrade;
    }

    public String getCoffee(){
        return coffeeGrade;
    }
}
