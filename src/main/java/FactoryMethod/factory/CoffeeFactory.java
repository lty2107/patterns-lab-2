package FactoryMethod.factory;

public class CoffeeFactory {
    public MyCoffee prepareCoffee(CoffeeGrade coffeeGrade) throws IllegalAccessException {
        switch (coffeeGrade){
            case Americano -> {
                return new MyCoffee(CoffeeGrade.Americano.getCoffee());
            }
            case Cappuccino -> {
                return new MyCoffee(CoffeeGrade.Cappuccino.getCoffee());
            }
            case Espresso -> {
                return new MyCoffee(CoffeeGrade.Espresso.getCoffee());
            }
            case Latte -> {
                return new MyCoffee(CoffeeGrade.Latte.getCoffee());
            }
            case Frappe -> {
                return new MyCoffee(CoffeeGrade.Frappe.getCoffee());
            }
        }
        throw new IllegalAccessException("Такого кофе нет в продаже");
    }
}
