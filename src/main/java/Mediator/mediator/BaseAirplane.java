package Mediator.mediator;

public class BaseAirplane implements Airplane{
    private String name;
    BaseDispatcher baseDispatcher;

    public BaseAirplane(String name, BaseDispatcher baseDispatcher) {
        this.name = name;
        this.baseDispatcher = baseDispatcher;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void sendMessage(String message) {
        baseDispatcher.sendMessage(message, this);
    }

    @Override
    public void getMessage(String message) {
        System.out.println(this.name + " получил сообщение: " + message + ".");
    }
}
