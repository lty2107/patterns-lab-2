package Mediator.mediator;

public interface Dispatcher {
    void addUser(BaseAirplane baseAirplane);
    void sendMessage(String message, BaseAirplane airplane);
    void getMessage(String message, BaseAirplane airplane);
}
