package Mediator.mediator;

import java.util.ArrayList;
import java.util.List;

public class BaseDispatcher implements Dispatcher{
    List<BaseAirplane> airplaneList = new ArrayList<>();
    private String name;

    public void setName(String name){
        this.name = name;
    }

    public void addUser(BaseAirplane airplane){
        this.airplaneList.add(airplane);
    }

    @Override
    public void sendMessage(String message, BaseAirplane airplane) {
        getMessage(message, airplane);
        for(Airplane a : airplaneList){
            if(a != airplane){
                a.getMessage(message);
            }
        }
    }

    @Override
    public void getMessage(String message, BaseAirplane airplane) {
        System.out.println("Сообщение от " + airplane.getName() + " : " + message);
    }
}
