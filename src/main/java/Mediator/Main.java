package Mediator;

import Mediator.mediator.Airplane;
import Mediator.mediator.BaseAirplane;
import Mediator.mediator.BaseDispatcher;
import Mediator.mediator.Dispatcher;

public class Main{
    public static void main(String[] args) {
        BaseDispatcher baseDispatcher = new BaseDispatcher();
        baseDispatcher.setName("Алексей");

        BaseAirplane airplane1 = new BaseAirplane("Самолет 1", baseDispatcher);
        BaseAirplane airplane2 = new BaseAirplane("Самолет 2", baseDispatcher);
        baseDispatcher.addUser(airplane1);
        baseDispatcher.addUser(airplane2);

        airplane1.sendMessage(airplane1.getName() + " готовится к взлету");
        airplane2.sendMessage(airplane2.getName() + " готовится к посадке");
    }
}
