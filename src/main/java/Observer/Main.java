package Observer;

import Observer.observer.Magazine;
import Observer.observer.Subscriber;

public class Main {
    public static void main(String[] args) {
        Magazine magazine = new Magazine("Комсомольская правда", "Выпуск 1");

        Subscriber firstSubscriber = new Subscriber("Геннадий");
        Subscriber secondSubscriber = new Subscriber("Алексей");

        magazine.addObserver(firstSubscriber);
        magazine.addObserver(secondSubscriber);

        magazine.setNewIssue("Выпуск 2");
    }
}
