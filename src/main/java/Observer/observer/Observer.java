package Observer.observer;

public interface Observer {
    void handlerEvent(String magazineName, String newIssue);
}
