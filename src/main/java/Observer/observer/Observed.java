package Observer.observer;

import java.util.List;

public interface Observed {
    void addObserver(Observer observer);
    void removeObserver(Observer observer);
    void clearObservers(List<Observer> subscribers);
    void notifyObservers();
}
