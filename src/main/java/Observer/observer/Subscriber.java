package Observer.observer;

public class Subscriber implements Observer{
    private String name;

    public Subscriber(String name) {
        this.name = name;
    }

    @Override
    public void handlerEvent(String magazineName, String newIssue) {
        System.out.println("Дорогой " + name +", вышел новый выпуск нашего журнала " + magazineName + ":" + newIssue);
    }
}
