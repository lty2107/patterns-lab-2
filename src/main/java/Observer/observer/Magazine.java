package Observer.observer;

import java.util.ArrayList;
import java.util.List;

public class Magazine implements Observed{
    private String magazineName;
    private String newIssue;

    public Magazine(String magazineName, String newIssue) {
        this.magazineName = magazineName;
        this.newIssue = newIssue;
    }

    public String getMagazineName() {
        return magazineName;
    }

    public String getNewIssue() {
        return newIssue;
    }

    public void setMagazineName(String magazineName) {
        this.magazineName = magazineName;
    }

    public void setNewIssue(String newIssue) {
        this.newIssue = newIssue;
        notifyObservers();
    }

    List<Observer> subscribers = new ArrayList<>();
    @Override
    public void addObserver(Observer observer) {
        this.subscribers.add(observer);
    }

    @Override
    public void removeObserver(Observer observer) {
        this.subscribers.remove(observer);
    }

    @Override
    public void clearObservers(List<Observer> subscribers) {
        this.subscribers.clear();
    }

    @Override
    public void notifyObservers() {
        for(Observer observer: subscribers){
            observer.handlerEvent(this.magazineName, this.newIssue);
        }
    }
}
