package Prototype.prototype;

public class Rhombus implements CloneShape{
    private int width;
    private int height;
    private int angle;

    public Rhombus(int width, int height, int angle) {
        this.width = width;
        this.height = height;
        this.angle = angle;
    }

    @Override
    public CloneShape clone(){
        return new Rhombus(width, height, this.angle);
    }

    @Override
    public String toString() {
        return "Ромб{" +
                "ширина=" + width +
                ", высота=" + height +
                ", угол=" + angle +
                '}';
    }
}
