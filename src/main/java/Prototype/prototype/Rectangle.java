package Prototype.prototype;

public class Rectangle implements CloneShape{
    private int width;
    private int height;

    public Rectangle(int width, int height) {
        this.width = width;
        this.height = height;
    }

    @Override
    public String toString() {
        return "Прямоугольник{" +
                "ширина=" + width +
                ", высота=" + height +
                '}';
    }

    @Override
    public CloneShape clone() {
        return new Rectangle(this.width, this.height);
    }
}
