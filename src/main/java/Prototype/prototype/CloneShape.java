package Prototype.prototype;

public interface CloneShape {
    CloneShape clone();
}
