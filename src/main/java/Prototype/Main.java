package Prototype;

import Prototype.prototype.CloneShape;
import Prototype.prototype.Rectangle;
import Prototype.prototype.Rhombus;

public class Main {
    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle(10, 20);
        CloneShape cloneShape = rectangle.clone();
        System.out.println(rectangle);
        System.out.println(cloneShape);

        Rhombus rhombus = new Rhombus(10, 20, 45);
        CloneShape cloneShape1 = rhombus.clone();
        System.out.println(rhombus);
        System.out.println(cloneShape1);
    }
}
