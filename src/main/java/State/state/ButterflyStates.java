package State.state;

public class ButterflyStates {
    private Stages stages;

    public ButterflyStates(Stages stages) {
        this.stages = stages;
    }

    public void setStages(Stages stages) {
        this.stages = stages;
    }

    public void grow(){
        if(stages instanceof Caterpillar){
            setStages(new Cocoon());
        } else if(stages instanceof Cocoon){
            setStages(new Butterfly());
        } else if(stages instanceof Butterfly){
            System.out.println("Рост завершен");
        }
    }

    public void doIt(){
        stages.doIt();
    }
}
