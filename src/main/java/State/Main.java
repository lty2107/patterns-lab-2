package State;

import State.state.ButterflyStates;
import State.state.Caterpillar;
import State.state.Stages;

public class Main {
    public static void main(String[] args) {
        Stages stages = new Caterpillar();
        ButterflyStates butterflyStates = new ButterflyStates(stages);
        for (int i = 0; i < 3; i++) {
            butterflyStates.doIt();
            butterflyStates.grow();
        }
    }
}
