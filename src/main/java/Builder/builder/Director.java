package Builder.builder;

public class Director {

    public void createCrystalGlass(GlassBuilder glassBuilder){
        glassBuilder.setManufacturer("Пензенский хрустальный завод");
        glassBuilder.setGlassType(GlassType.Crystal);
        glassBuilder.setPrice(1200);
    }

    public void createCeramicGlass(GlassBuilder glassBuilder){
        glassBuilder.setManufacturer("Московский керамический завод");
        glassBuilder.setGlassType(GlassType.Ceramic);
        glassBuilder.setPrice(600);
    }

    public void createPlasticGlass(GlassBuilder glassBuilder){
        glassBuilder.setManufacturer("Саранский пластковый завод");
        glassBuilder.setGlassType(GlassType.Plastic);
        glassBuilder.setPrice(900);
    }
}
