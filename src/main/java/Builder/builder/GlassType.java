package Builder.builder;

public enum GlassType {
    Crystal,
    Ceramic,
    Plastic
}
