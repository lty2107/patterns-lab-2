package Builder.builder;

public interface Builder {
    void setManufacturer(String manufacturer);
    void setGlassType(GlassType glassType);
    void setPrice(int price);
    Glass getGlass();
}
