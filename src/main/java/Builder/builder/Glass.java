package Builder.builder;

public class Glass {
    private final String manufacturer;
    private final GlassType glassType;
    private final int price;

    public Glass(String manufacturer, GlassType glassType, int price) {
        this.manufacturer = manufacturer;
        this.glassType = glassType;
        this.price = price;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public GlassType getGlassType() {
        return glassType;
    }

    public int getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "Бокал{" +
                "производитель='" + manufacturer + '\'' +
                ", тип материала=" + glassType +
                ", цена=" + price +
                '}';
    }
}
