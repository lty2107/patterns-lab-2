package Builder.builder;

public class GlassBuilder implements Builder{
    private String manufacturer;
    private GlassType glassType;
    private int price;

    @Override
    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    @Override
    public void setGlassType(GlassType glassType) {
        this.glassType = glassType;
    }

    @Override
    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public Glass getGlass() {
        return new Glass(manufacturer, glassType, price);
    }
}
