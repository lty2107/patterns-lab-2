package Builder;

import Builder.builder.Director;
import Builder.builder.Glass;
import Builder.builder.GlassBuilder;

public class Main {
    public static void main(String[] args) {
        GlassBuilder glassBuilder = new GlassBuilder();
        Director director = new Director();
        director.createPlasticGlass(glassBuilder);
        Glass glass = glassBuilder.getGlass();
        System.out.println(glass.toString());
    }
}
