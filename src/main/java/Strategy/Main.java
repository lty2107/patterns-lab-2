package Strategy;

import Strategy.strategy.CashPayment;
import Strategy.strategy.CoinsPayment;
import Strategy.strategy.ShopPayment;
import Strategy.strategy.TransferPayment;

public class Main {
    public static void main(String[] args) {
        ShopPayment shopPayment = new ShopPayment();
        shopPayment.setPayment(new CashPayment());
        shopPayment.makePayment();

        shopPayment.setPayment(new CoinsPayment());
        shopPayment.makePayment();

        shopPayment.setPayment(new CashPayment());
        shopPayment.makePayment();

        shopPayment.setPayment(new TransferPayment());
        shopPayment.makePayment();
    }
}
