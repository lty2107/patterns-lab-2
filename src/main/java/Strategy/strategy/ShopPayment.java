package Strategy.strategy;

public class ShopPayment {
    private Payment payment;

    public void setPayment(Payment payment) {
        this.payment = payment;
    }

    public void makePayment(){
        this.payment.payment();
    }
}
