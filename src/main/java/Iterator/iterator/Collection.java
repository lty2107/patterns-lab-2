package Iterator.iterator;

public interface Collection {
    Iterator getIterator();
}
