package Iterator.iterator;

public interface Iterator {
    Boolean hasNext();
    Object next();
}
