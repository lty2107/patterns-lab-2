package Iterator.iterator;

public class DayWeek implements Collection{
    private String day;
    private String[] tasks;


    public void setDay(String day) {
        this.day = day;
    }

    public void setTasks(String[] tasks) {
        this.tasks = tasks;
    }

    public String getDay() {
        return day;
    }

    public String[] getTasks() {
        return tasks;
    }

    public DayWeek(String day, String[] tasks) {
        this.day = day;
        this.tasks = tasks;
    }

    @Override
    public Iterator getIterator() {
        return new TasksIterator();
    }

    private class TasksIterator implements Iterator{
        int index;

        @Override
        public Boolean hasNext() {
            return index < tasks.length;
        }

        @Override
        public Object next() {
            return tasks[index++];
        }
    }
}
