package Iterator;

import Iterator.iterator.DayWeek;
import Iterator.iterator.Iterator;

public class Main {
    public static void main(String[] args) {
        String[] tasks = {"забрать посылку с почты", "сходить в спортзал", "встреча с друзьями"};
        DayWeek dayWeek = new DayWeek("Пятница", tasks);

        Iterator iterator = dayWeek.getIterator();
        System.out.println("День недели:" + dayWeek.getDay());
        System.out.println("Задачи:");
        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }
    }
}
