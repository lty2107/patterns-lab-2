package Template.template;

public class GermanFlag extends Flag{
    private String countryName;

    public GermanFlag(String countryName) {
        this.countryName = countryName;
    }

    @Override
    public String getCountryName() {
        return this.countryName;
    }

    @Override
    public void drawFirstStrip() {
        System.out.println("Черная полоса");
    }

    @Override
    public void drawSecondStrip() {
        System.out.println("Красная полоса");
    }

    @Override
    public void drawThirdStrip() {
        System.out.println("Желтая полоса");
    }
}
