package Template.template;

public class RussianFlag extends Flag{
    private String countryName;

    public RussianFlag(String countryName) {
        this.countryName = countryName;
    }

    @Override
    public String getCountryName() {
        return this.countryName;
    }

    @Override
    public void drawFirstStrip() {
        System.out.println("Белая полоса");
    }

    @Override
    public void drawSecondStrip() {
        System.out.println("Синяя полоса");
    }

    @Override
    public void drawThirdStrip() {
        System.out.println("Красная полоса\n");
    }
}
