package Template.template;

public abstract class Flag {

    public void drawFlag(){
        System.out.println("Флаг " + getCountryName());
        drawFirstStrip();
        drawSecondStrip();
        drawThirdStrip();
    }

    abstract String getCountryName();
    abstract void drawFirstStrip();
    abstract void drawSecondStrip();
    abstract void drawThirdStrip();
}
