package Template;

import Template.template.Flag;
import Template.template.GermanFlag;
import Template.template.RussianFlag;

public class Main {
    public static void main(String[] args) {
        Flag flag = new RussianFlag("России");
        flag.drawFlag();
        Flag flag1 = new GermanFlag("Германии");
        flag1.drawFlag();
    }
}
